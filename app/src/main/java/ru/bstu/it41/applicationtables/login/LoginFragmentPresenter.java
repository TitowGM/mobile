package ru.bstu.it41.applicationtables.login;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;

import etr.android.reamp.mvp.ReampPresenter;
import ru.bstu.it41.applicationtables.any.UserRequest;
import ru.istok.myapp.HTTPRequest.PerformerRequest;

/**
 * Created by Герман on 08.10.2017.
 */

public class LoginFragmentPresenter extends ReampPresenter<LoginFragmentState> {

    @Override
    public void onPresenterCreated() {
        super.onPresenterCreated();

        //Это временно, само собой.
        getStateModel().setLogin("agggr@mail.ru");
        getStateModel().setPassword("1234");

        getStateModel().setShowProgress(false);
        getStateModel().setLoggedIn(null);
        getStateModel().setLostConnection(false);

        sendStateModel();
    }

    public void loginChanged(String login) {
        getStateModel().setLogin(login);
        //sendStateModel();
    }

    public void passwordChanged(String password) {
        getStateModel().setPassword(password);
        //sendStateModel();
    }

    public void login() {

        getStateModel().setShowProgress(true);
        getStateModel().setLoggedIn(null);
        sendStateModel();

        performLogin();
    }

    void performLogin() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(UserRequest.PARAM_EMAIL,getStateModel().getLogin()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_PASSWORD,getStateModel().getPassword()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_MOBILE,"mobile"));

        UserRequest request = new UserRequest(UserRequest.URL_LOGIN,params);

        PerformerRequest performer = new PerformerRequest(request,true){
            @Override
            public void onGettingToken(String[] token) {
                getStateModel().setShowProgress(false);
                getStateModel().setLoggedIn(token[0].equals("success"));
                getStateModel().setLostConnection(false);
                if(token.length>1)
                    getStateModel().message = token[1];
                sendStateModel();
            }
            @Override
            public void onLessConnection(String[] messages) {
                getStateModel().setShowProgress(false);
                getStateModel().setLoggedIn(null);
                getStateModel().setLostConnection(true);
                if(messages.length>1)
                    getStateModel().message = messages[1];

                sendStateModel();
            }

            @Override
            public void onCompleteRequest(JSONArray array) {

            }
        };
        performer.execute();
    }
}
