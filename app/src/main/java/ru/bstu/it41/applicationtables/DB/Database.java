package ru.bstu.it41.applicationtables.DB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.bstu.it41.applicationtables.DownloadActivity;
import ru.bstu.it41.applicationtables.any.UserRequest;
import ru.istok.myapp.HTTPRequest.PerformerRequest;


public class Database {
    private static final String DB_NAME = "mydb";
    private static final int DB_VERSION = 1;
    private static final String DB_TABLE = "mytab";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TXT = "txt";
    private static final String DB_CREATE = "create table " + DB_TABLE +
            "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TXT + " text" + ");";
    private final Context mCtx;
    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    Object buffer;
    public Boolean downloaded = null;
    public Boolean zone_downloaded = false;
    public Boolean country_downloaded = false;

    public void onDownloaded(Boolean download){
        ((DownloadActivity)buffer).onDownloadDB(download);
    }

    public Database(Context ctx, Object buffer) {
        mCtx = ctx;
        this.buffer = buffer;
    }
    public Database(Context ctx) {
        mCtx = ctx;
    }

    public void open() {
        mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    public void upgradeDB(){
        mDB = mDBHelper.getWritableDatabase();
        mDBHelper.onUpgrade(mDB,0,0);
    }

    public void close() {
        if (mDBHelper != null) mDBHelper.close();
    }

    public ArrayList<NameValuePair> getCountries() {
        ArrayList<NameValuePair> countries = new ArrayList<NameValuePair>();
        String selectQuery = "SELECT * FROM " + DBHelper.TABLE_COUNTRY;
        Cursor cursor = mDB.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                Integer id = cursor.getInt(0);
                String name = cursor.getString(1);
                NameValuePair country = new BasicNameValuePair(id.toString(), name);
                countries.add(country);
            } while (cursor.moveToNext());
        }
        return countries;
    }

    public ArrayList<NameValuePair> getZoneByCountryId(Integer countryID){
        ArrayList<NameValuePair> zones = new ArrayList<NameValuePair>();
        String selectQuery = "SELECT "+DBHelper.ID_ZONE + "," +DBHelper.NAME_ZONE +" FROM " + DBHelper.TABLE_ZONE
                + " WHERE "+DBHelper.COUNTRY_ID_ZONE +" = "+ countryID.toString();
        Cursor cursor = mDB.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Integer id = cursor.getInt(0);
                String name = cursor.getString(1);
                NameValuePair country = new BasicNameValuePair(id.toString(), name);
                zones.add(country);
            } while (cursor.moveToNext());
        }
        return zones;
    }

    private class DBHelper extends SQLiteOpenHelper {

        protected static final String TABLE_COUNTRY = "country";
        protected static final String ID_COUNTRY = "country_id";
        protected static final String NAME_COUNTRY = "name";
        protected static final String ISO_CODE_2 = "iso_code_2";
        protected static final String ISO_CODE_3 = "iso_code_3";
        protected static final String POSTCODE_REQUIRED = "postcode_required";

        protected static final String TABLE_ZONE = "zone";
        protected static final String ID_ZONE = "zone_id";
        protected static final String NAME_ZONE = "name";
        protected static final String COUNTRY_ID_ZONE = "country_id";

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            downloaded = false;
            String CREATE_COUNTRY_TABLE = "CREATE TABLE " + TABLE_COUNTRY + "("
                    + ID_COUNTRY + " INTEGER PRIMARY KEY,"
                    + NAME_COUNTRY + " TEXT," + ISO_CODE_2 + " TEXT," + ISO_CODE_3 + " TEXT,"
                    + POSTCODE_REQUIRED + " INTEGER" + ")";
            db.execSQL(CREATE_COUNTRY_TABLE);

            String CREATE_ZONE_TABLE = "CREATE TABLE " + TABLE_ZONE + "("
                    + ID_ZONE + " INTEGER PRIMARY KEY,"
                    + NAME_ZONE + " TEXT," + COUNTRY_ID_ZONE + " INTEGER" + ")";
            db.execSQL(CREATE_ZONE_TABLE);

            fillDB(db);
            downloaded = true;
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_COUNTRY);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ZONE);
            onCreate(db);
        }

        private void fillDB(SQLiteDatabase db) {
            ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(UserRequest.PARAM_MOBILE, "0"));

            UserRequest request = new UserRequest(UserRequest.URL_COUNTRIES_ZONES, params);

            PerformerRequest performer = new PerformerRequest(request, false, db) {
                @Override
                public void onCompleteRequest(JSONArray array) {
                    try {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            ContentValues values = new ContentValues();
                            values.put(ID_COUNTRY, c.getString(ID_COUNTRY));
                            values.put(NAME_COUNTRY, c.getString(NAME_COUNTRY));
                            values.put(ISO_CODE_2, c.getString(ISO_CODE_2));
                            values.put(ISO_CODE_3, c.getString(ISO_CODE_3));
                            values.put(POSTCODE_REQUIRED, c.getString(POSTCODE_REQUIRED));

                            ((SQLiteDatabase) this.buffer).insert(TABLE_COUNTRY, null, values);
                        }
                        country_downloaded = true;
                        if(country_downloaded && zone_downloaded)
                            onDownloaded(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        onDownloaded(false);
                    }
                }

                @Override
                public void onLessConnection(String[] messages) {
                    onDownloaded(false);
                }

                @Override
                public void onGettingToken(String[] token) {

                }
            };
            performer.execute();

            params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(UserRequest.PARAM_MOBILE, "1"));
            UserRequest request_zones = new UserRequest(UserRequest.URL_COUNTRIES_ZONES, params);

            performer = new PerformerRequest(request_zones, false, db) {
                @Override
                public void onCompleteRequest(JSONArray array) {
                    try {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            ContentValues values = new ContentValues();
                            values.put(ID_ZONE, c.getString(ID_ZONE));
                            values.put(ID_COUNTRY, c.getString(ID_COUNTRY));
                            values.put(NAME_ZONE, c.getString(NAME_ZONE));

                            ((SQLiteDatabase) this.buffer).insert(TABLE_ZONE, null, values);
                        }
                        zone_downloaded = true;
                        if(country_downloaded && zone_downloaded)
                            onDownloaded(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        onDownloaded(false);
                    }
                }

                @Override
                public void onLessConnection(String[] messages) {
                    onDownloaded(false);
                }

                @Override
                public void onGettingToken(String[] token) {

                }
            };
            performer.execute();
        }

    }
}
