package ru.bstu.it41.applicationtables.any;

import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;

import java.util.ArrayList;

import ru.istok.myapp.HTTPRequest.Request;

/**
 * Created by Герман on 05.10.2017.
 */

public final class UserRequest extends Request {
    private static String prefix = "http://192.168.2.107";
    //private static String prefix = "http://192.168.43.245";
    public static final String URL_LOGIN = prefix + "/index.php?route=account/login";
    public static final String URL_REGISTER = prefix + "/index.php?route=account/register";
    public static final String URL_COUNTRIES_ZONES = prefix + "/index.php?route=account/getlocalisation";
    public static final String URL_LOGOUT = prefix + "/index.php?route=account/logout";

    public static final String TAG_SUCCESS = "success";

    //нужны для авторизации
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_MOBILE = "mobile";

    //недостающие для регистрации + пароль, e-mail
    public static final String PARAM_FIRSTNAME = "firstname";
    public static final String PARAM_LASTNAME = "lastname";
    public static final String PARAM_TELEPHONE = "telephone";
    public static final String PARAM_ADDRESS_1 = "address_1";
    public static final String PARAM_CITY = "city";
    public static final String PARAM_COUNTRY = "country_id";
    public static final String PARAM_ZONE = "zone_id";
    public static final String PARAM_CONFIRM = "confirm";
    public static final String PARAM_AGREE = "agree";
    public static final String PARAM_FAX = "fax";
    public static final String PARAM_COMPANY= "company";
    public static final String PARAM_ADDRESS_2= "address_2";
    public static final String PARAM_POSTCODE= "postcode";

    public UserRequest(String url, ArrayList<NameValuePair> params){
        super(url,params);
    }

    public UserRequest(String url, ArrayList<NameValuePair> params,String method){
        super(url,method,params);
    }
    public UserRequest(String url){
        super(url);
    }
}
