package ru.bstu.it41.applicationtables.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import etr.android.reamp.mvp.ReampFragment;
import etr.android.reamp.mvp.ReampPresenter;
import ru.bstu.it41.applicationtables.MainActivity;
import ru.bstu.it41.applicationtables.main.R;
/**
 * Created by Герман on 08.10.2017.
 */

public class LoginFragment extends ReampFragment<LoginFragmentPresenter, LoginFragmentState> {
    //Элементы управления
    EditText editLogin;
    EditText editPassword;
    Button buttonLogin;
    //view-шки
    TextView textFailedLogin;
    TextView textSuccessLogin;
    TextView textLostConnection;
    TextView message;
    MainActivity parent;
    //
    ProgressBar progressBar;

    @Override
    public void onStateChanged(LoginFragmentState stateModel) {
        editLogin.setText(stateModel.getLogin());
        editPassword.setText(stateModel.getPassword());
        message.setText(stateModel.message);

        buttonLogin.setEnabled(stateModel.isLoginActionEnabled());

        textFailedLogin.setVisibility(stateModel.showFailedLogin()?View.VISIBLE:View.INVISIBLE);
        textSuccessLogin.setVisibility(stateModel.showSuccessLogin()?View.VISIBLE:View.INVISIBLE);

        textLostConnection.setVisibility(stateModel.getLostConnection()?View.VISIBLE:View.INVISIBLE);

        progressBar.setVisibility(stateModel.getShowProgress()?View.VISIBLE:View.INVISIBLE);

        parent.getPresenter().onAuthorizedChanged(stateModel.showSuccessLogin());
    }
    @Override
    public ReampPresenter<LoginFragmentState> onCreatePresenter() {
        return new LoginFragmentPresenter();
    }

    @Override
    public LoginFragmentState onCreateStateModel() {
        return new LoginFragmentState();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parent = (MainActivity) getActivity();

        editLogin = (EditText)view.findViewById(R.id.editLogin);
        editPassword = (EditText)view.findViewById(R.id.editPassword);
        buttonLogin = (Button)view.findViewById(R.id.buttonLogin);

        textFailedLogin = (TextView)view.findViewById(R.id.text_login_error);
        textSuccessLogin = (TextView)view.findViewById(R.id.text_login_success);
        textLostConnection = (TextView)view.findViewById(R.id.text_lost_connection);
        message = (TextView)view.findViewById(R.id.text_message);

        progressBar = (ProgressBar)view.findViewById(R.id.progress);

        editLogin.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().loginChanged(charSequence.toString());
            }
        });
        editPassword.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().passwordChanged(charSequence.toString());
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().login();
            }
        });
    }
}


