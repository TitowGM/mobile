package ru.bstu.it41.applicationtables.register;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

import etr.android.reamp.mvp.ReampFragment;
import etr.android.reamp.mvp.ReampPresenter;
import ru.bstu.it41.applicationtables.DB.Database;
import ru.bstu.it41.applicationtables.MainActivity;
import ru.bstu.it41.applicationtables.main.R;

import static java.lang.Integer.parseInt;

/**
 * Created by Герман on 18.10.2017.
 */

public class RegisterFragment extends ReampFragment<RegisterFragmentPresenter, RegisterFragmentState> {
    Database db;
    ArrayList<NameValuePair> countries;
    ArrayList<NameValuePair> zones;
    //Элементы управления
    Button buttonLogin;
    EditText editFirstname;
    EditText editLastname;
    EditText editEmail;
    EditText editTelephone;
    EditText editAddress_1;
    EditText editCity;
    Spinner listCountry;
    Spinner listZone;
    EditText editPassword;
    EditText editConfirm;
    CheckBox checkAgree;
    //view-шки
    TextView message;
    TextView textSuccessLogin;
    TextView textLostConnection;
    TextView textFailedLogin;

    MainActivity parent;
    //
    ProgressBar progressBar;

    @Override
    public void onStateChanged(RegisterFragmentState stateModel) {
        //Я намерено не стал записывать в edit-ы значения из stateModel.
        //Вообще с ними как-то непонятно. Можно либо писать в presenter-е sendModelState,
        //либо тут заполнять edit-ы из значений stateModel, и то и то разом делать нельзя.
        //Пока что я лишил себя возможности программно менять значения edit-ов. Вроде мелочь.

        //Цвет будет красным там, где ошибка, все элементарно.
        editFirstname.setBackgroundColor(stateModel.isFail_firstname()?Color.RED:Color.WHITE);
        editLastname.setBackgroundColor(stateModel.isFail_lastname()?Color.RED:Color.WHITE);
        editEmail.setBackgroundColor(stateModel.isFail_email()?Color.RED:Color.WHITE);
        editTelephone.setBackgroundColor(stateModel.isFail_telephone()?Color.RED:Color.WHITE);
        editAddress_1.setBackgroundColor(stateModel.isFail_address_1()?Color.RED:Color.WHITE);
        editCity.setBackgroundColor(stateModel.isFail_city()?Color.RED:Color.WHITE);
        editPassword.setBackgroundColor(stateModel.isFail_password()?Color.RED:Color.WHITE);
        editConfirm.setBackgroundColor(stateModel.isFail_confirm()?Color.RED:Color.WHITE);


        progressBar.setVisibility(stateModel.getShowProgress()?View.VISIBLE:View.INVISIBLE);
        textFailedLogin.setVisibility(stateModel.showFailedLogin()?View.VISIBLE:View.INVISIBLE);
        textSuccessLogin.setVisibility(stateModel.showSuccessLogin()?View.VISIBLE:View.INVISIBLE);

        parent.getPresenter().onAuthorizedChanged(stateModel.showSuccessLogin());

        textLostConnection.setVisibility(stateModel.getLostConnection()?View.VISIBLE:View.INVISIBLE);

        checkAgree.setChecked(stateModel.isAgree());
        //Мегакрутая отладка
        message.setText(stateModel.message);
    }
    @Override
    public ReampPresenter<RegisterFragmentState> onCreatePresenter() {
        return new RegisterFragmentPresenter();
    }

    @Override
    public RegisterFragmentState onCreateStateModel() {
        return new RegisterFragmentState();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    private ArrayAdapter<NameValuePair> getZoneAdapter(Integer countryID, Database db){
        ArrayList<NameValuePair> zones = db.getZoneByCountryId(countryID);

        ArrayAdapter<NameValuePair> adapter2 = new ArrayAdapter<NameValuePair>(getContext(),android.R.layout.simple_list_item_1,zones){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                NameValuePair zone = getItem(position);

                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext())
                            .inflate(android.R.layout.simple_list_item_1, null);
                }
                ((TextView) convertView.findViewById(android.R.id.text1))
                        .setText(zone.getValue());
                return convertView;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                NameValuePair zone = getItem(position);

                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext())
                            .inflate(android.R.layout.simple_list_item_1, null);
                }
                ((TextView) convertView.findViewById(android.R.id.text1))
                        .setText(zone.getValue());
                return convertView;
            }
        };
        return adapter2;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editFirstname = (EditText)view.findViewById(R.id.edit_firstname);
        editLastname = (EditText)view.findViewById(R.id.edit_lastname);
        editEmail = (EditText)view.findViewById(R.id.edit_email);
        editTelephone = (EditText)view.findViewById(R.id.edit_telephone);
        editAddress_1 = (EditText)view.findViewById(R.id.edit_address_1);
        editCity = (EditText)view.findViewById(R.id.edit_city);

        parent = (MainActivity) getActivity();

        listCountry = (Spinner)view.findViewById(R.id.list_country);
        listZone = (Spinner)view.findViewById(R.id.list_zone);

        db = new Database(getContext());
        //Заполнение списков
        db.open();

        //адаптер для стран
        countries = db.getCountries();

        ArrayAdapter<NameValuePair> adapter = new ArrayAdapter<NameValuePair>(getContext(),android.R.layout.simple_spinner_item,countries){
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                NameValuePair country = getItem(position);

                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext())
                            .inflate(android.R.layout.simple_list_item_1, null);
                }
                ((TextView) convertView.findViewById(android.R.id.text1))
                        .setText(country.getValue());
                return convertView;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                NameValuePair country = getItem(position);

                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext())
                            .inflate(android.R.layout.simple_list_item_1, null);
                }
                ((TextView) convertView.findViewById(android.R.id.text1))
                        .setText(country.getValue());
                return convertView;
            }
        };

        listCountry.setAdapter(adapter);

        /*try {
            listCountry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    int country_id = parseInt(((NameValuePair) adapterView.getItemAtPosition(i)).getName());
                    db.open();
                    getPresenter().countryChanged(country_id);
                    listZone.setAdapter(getZoneAdapter(country_id, db));
                    db.close();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }*/
        listCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int country_id = parseInt(((NameValuePair) adapterView.getItemAtPosition(i)).getName());
                db.open();
                getPresenter().countryChanged(country_id);
                listZone.setAdapter(getZoneAdapter(country_id, db));
                /*try {
                listZone.getAdapter().notify();
                zones = db.getZoneByCountryId(country_id);

                    listZone.getAdapter().notifyAll();
                }catch(Exception e){
                    e.printStackTrace();
                }*/
                db.close();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        listZone.setAdapter(getZoneAdapter(parseInt(countries.get(0).getName()),db));

        listZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int zone_id =parseInt(((NameValuePair)adapterView.getItemAtPosition(i)).getName());
                getPresenter().zoneChanged(zone_id);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        db.close();

        editPassword = (EditText)view.findViewById(R.id.edit_password);
        editConfirm = (EditText)view.findViewById(R.id.edit_confirm);

        textFailedLogin = (TextView)view.findViewById(R.id.text_login_error);
        textSuccessLogin = (TextView)view.findViewById(R.id.text_login_success);
        textLostConnection = (TextView)view.findViewById(R.id.text_lost_connection);

        buttonLogin = (Button)view.findViewById(R.id.buttonLogin);

        checkAgree = (CheckBox)view.findViewById(R.id.checkBox_agree);

        message = (TextView)view.findViewById(R.id.text_message);

        progressBar = (ProgressBar)view.findViewById(R.id.progress);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().register();
            }
        });

        //Куча однотипных обработчиков. Как же это достало...
        editFirstname.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().firstnameChanged(charSequence.toString());
            }
        });
        editLastname.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().lastnameChanged(charSequence.toString());
            }
        });
        editEmail.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().emailChanged(charSequence.toString());
            }
        });
        editTelephone.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().telephoneChanged(charSequence.toString());
            }
        });
        editAddress_1.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().address_1Changed(charSequence.toString());
            }
        });
        editCity.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().cityChanged(charSequence.toString());
            }
        });
        editPassword.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().passwordChanged(charSequence.toString());
            }
        });
        editConfirm.addTextChangedListener(new MainActivity.SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getPresenter().confirmChanged(charSequence.toString());
            }
        });

        checkAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().agreeChanged(((CheckBox)view).isChecked());
            }
        });
    }
}
