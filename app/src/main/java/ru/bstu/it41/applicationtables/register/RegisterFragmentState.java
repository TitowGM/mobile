package ru.bstu.it41.applicationtables.register;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import etr.android.reamp.mvp.SerializableStateModel;

import static java.lang.Integer.parseInt;

/**
 * Created by Герман on 18.10.2017.
 */

public class RegisterFragmentState extends SerializableStateModel {

    private Boolean showProgress;
    private Boolean lostConnection;

    private Boolean loggedIn;
    public String message;

    //В этом перечне нет необязательных полей, которые, тем не менее,
    //присутствуют на сайте. Придется добавить позже.

    private String firstname;
    private Boolean fail_firstname;
    private String lastname;
    private Boolean fail_lastname;
    private String email;
    private Boolean fail_email;
    private String telephone;
    private Boolean fail_telephone;
    private String address_1;
    private Boolean fail_address_1;
    private String city;
    private Boolean fail_city;
    //Может можно и кодами хранить
    private Integer country_id;
    private Boolean fail_country;
    private Integer zone_id;
    private Boolean fail_zone;

    private String password;
    private Boolean fail_password;

    private String confirm;
    private Boolean fail_confirm;

    private boolean agree;

    public boolean isAgree() {
        return agree;
    }

    public void setAgree(boolean agree) {
        this.agree = agree;
    }

    //true если все хорошо и можно отправлять данные на сервер, иначе false
    public Boolean totalCheck(){

        if(fail_firstname == null)
            fail_firstname = true;
        if(fail_lastname == null)
            fail_lastname = true;
        if(fail_email == null)
            fail_email = true;
        if(fail_telephone == null)
            fail_telephone = true;
        if(fail_address_1 == null)
            fail_address_1 = true;
        if(fail_city == null)
            fail_city = true;
        if(fail_country == null)
            fail_country = true;
        if(fail_zone == null)
            fail_zone = true;
        if(fail_password == null)
            fail_password = true;
        if(fail_confirm == null)
            fail_confirm = true;

        if(!fail_firstname && !fail_lastname && !fail_email && !fail_telephone
                && !fail_address_1 && !fail_city && !fail_country && !fail_zone
                && !fail_password && !fail_confirm)
            return true;
        else
            return false;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        if(firstname.length() < 1 || firstname.length()>32) {
            fail_firstname = true;
            this.firstname = firstname;
        }
        else {
            fail_firstname = false;
            this.firstname = firstname;
        }
    }

    public Boolean isFail_firstname() {
        return fail_firstname != null && fail_firstname;
    }

    public void setFail_firstname(Boolean fail_firstname) {
        this.fail_firstname = fail_firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        if(lastname.length() < 1 || lastname.length()>32) {
            fail_lastname = true;
            this.lastname = lastname;
        }
        else {
            fail_lastname = false;
            this.lastname = lastname;
        }
    }

    public Boolean isFail_lastname() {
        return fail_lastname != null && fail_lastname;
    }

    public void setFail_lastname(Boolean fail_lastname) {
        this.fail_lastname = fail_lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        //Регулярное выражение
        Pattern pattern = Pattern.compile(".+@.+\\..+");
        Matcher m = pattern.matcher(email);
        if(m.matches()) {
            this.email = email;
            fail_email = false;
        }
        else {
            fail_email = true;
            this.email = email;
        }
    }

    public Boolean isFail_email() {
        return fail_email != null && fail_email;
    }

    public void setFail_email(Boolean fail_email) {
        this.fail_email = fail_email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        if(telephone.length() < 3 || telephone.length() > 32){
            fail_telephone = true;
            this.telephone = telephone;
        }
        else {
            this.telephone = telephone;
            fail_telephone = false;
        }
    }

    public Boolean isFail_telephone() {
        return fail_telephone != null && fail_telephone;
    }

    public void setFail_telephone(Boolean fail_telephone) {
        this.fail_telephone = fail_telephone;
    }

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        if(address_1.length() < 3 || address_1.length() > 128){
            this.address_1 = address_1;
            fail_address_1 = true;
        }else {
            this.address_1 = address_1;
            fail_address_1 = false;
        }
    }

    public Boolean isFail_address_1() {
        return fail_address_1 != null && fail_address_1;
    }

    public void setFail_address_1(Boolean fail_address_1) {
        this.fail_address_1 = fail_address_1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if(city.length() < 2 || city.length() > 128){
            this.city = city;
            fail_city =  true;
        }else {
            this.city = city;
            fail_city = false;
        }
    }

    public Boolean isFail_city() {
        return fail_city != null && fail_city;
    }

    public void setFail_city(Boolean fail_city) {
        this.fail_city = fail_city;
    }

    public Integer getCountry_id() {
        return country_id;
    }

    public void setCountry(Integer country_id) {
        this.country_id = country_id;
        fail_country = false;
    }

    public void setCountry(String country_id) {
        this.country_id = parseInt(country_id);
        fail_country = false;
    }

    public Boolean isFail_country() {
        return fail_country != null && fail_country;
    }

    public void setFail_country(Boolean fail_country) {
        this.fail_country = fail_country;
    }

    public Integer getZone() {
        return zone_id;
    }

    public void setZone(Integer zone_id) {
        this.zone_id = zone_id;
        fail_zone = false;
    }
    public void setZone(String zone_id) {
        this.zone_id = parseInt(zone_id);
        fail_zone = false;
    }

    public Boolean isFail_zone() {
        return fail_zone != null && fail_zone;
    }

    public void setFail_zone(Boolean fail_zone) {
        this.fail_zone = fail_zone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if(password.length() < 4 || password.length() > 20) {
            this.password = password;
            fail_password = true;
        }else{
            this.password = password;
            fail_password = false;
        }
    }

    public Boolean isFail_password() {
        return fail_password != null && fail_password;
    }

    public void setFail_password(Boolean fail_password) {
        this.fail_password = fail_password;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        if(!confirm.equals(password)){
            this.confirm = confirm;
            fail_confirm = true;
        }else {
            this.confirm = confirm;
            fail_confirm = false;
        }
    }

    public Boolean isFail_confirm() {
        return fail_confirm!= null && fail_confirm;
    }

    public void setFail_confirm(Boolean fail_confirm) {
        this.fail_confirm = fail_confirm;
    }

    public Boolean showFailedLogin() {
        return loggedIn != null && !loggedIn;
    }

    public Boolean showSuccessLogin() {
        return loggedIn != null && loggedIn;
    }

    public Boolean getShowProgress() {
        return showProgress;
    }

    public void setLoggedIn(Boolean loggedIn){
        this.loggedIn = loggedIn;
    }

    public void setShowProgress(Boolean showProgress) {
        this.showProgress = showProgress;
    }

    public Boolean getLostConnection() {
        return lostConnection;
    }

    public void setLostConnection(Boolean lostConnection) {
        this.lostConnection = lostConnection;
    }
}
