package ru.bstu.it41.applicationtables;

import etr.android.reamp.mvp.SerializableStateModel;

/**
 * Created by Герман on 11.11.2017.
 */

public class MainActivityState extends SerializableStateModel {
    private Boolean authorized;

    public Boolean getAuthorized() {
        return authorized;
    }

    public void setAuthorized(Boolean authorized) {
        this.authorized = authorized;
    }
}
