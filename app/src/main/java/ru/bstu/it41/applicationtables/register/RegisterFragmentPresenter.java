package ru.bstu.it41.applicationtables.register;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;

import etr.android.reamp.mvp.ReampPresenter;
import ru.bstu.it41.applicationtables.any.UserRequest;
import ru.istok.myapp.HTTPRequest.PerformerRequest;

/**
 * Created by Герман on 18.10.2017.
 */

public class RegisterFragmentPresenter extends ReampPresenter<RegisterFragmentState> {

    @Override
    public void onPresenterCreated() {
        super.onPresenterCreated();

        //Это временно, само собой.

        getStateModel().setShowProgress(false);
        getStateModel().setLostConnection(false);
        getStateModel().setLoggedIn(null);

        //fail-ы
        getStateModel().setFail_firstname(null);
        getStateModel().setFail_lastname(null);
        getStateModel().setFail_email(null);
        getStateModel().setFail_telephone(null);
        getStateModel().setFail_address_1(null);
        getStateModel().setFail_city(null);
        getStateModel().setFail_country(null);
        getStateModel().setFail_zone(null);
        getStateModel().setFail_password(null);
        getStateModel().setFail_confirm(null);
        getStateModel().setAgree(false);

        sendStateModel();
    }

    public void register() {

        if(getStateModel().totalCheck()) {
            if(getStateModel().isAgree()) {
                getStateModel().setShowProgress(true);

                performRegister();
            }
            else{
                //Можно вывести другое сообщение
            }
        }else{
            //Ну может вывести какое сообщение
        }
        sendStateModel();
    }

    public void firstnameChanged(String firstname){
        getStateModel().setFirstname(firstname);
        sendStateModel();
    }

    public void lastnameChanged(String lastname){
        getStateModel().setLastname(lastname);
        sendStateModel();
    }

    public void emailChanged(String email){
        getStateModel().setEmail(email);
        sendStateModel();
    }

    public void telephoneChanged(String telephone){
        getStateModel().setTelephone(telephone);
        sendStateModel();
    }
    public void address_1Changed(String address_1){
        getStateModel().setAddress_1(address_1);
        sendStateModel();
    }
    public void cityChanged(String city){
        getStateModel().setCity(city);
        sendStateModel();
    }
    public void countryChanged(String country){
        getStateModel().setCountry(country);
        sendStateModel();
    }

    public void countryChanged(Integer country){
        getStateModel().setCountry(country);
        sendStateModel();
    }

    public void zoneChanged(String zone){
        getStateModel().setZone(zone);
        sendStateModel();
    }
    public void zoneChanged(int zone){
        getStateModel().setZone(zone);
        sendStateModel();
    }

    public void passwordChanged(String password){
        getStateModel().setPassword(password);
        sendStateModel();
    }
    public void confirmChanged(String confirm){
        getStateModel().setConfirm(confirm);
        sendStateModel();
    }

    public void agreeChanged(boolean agree)
    {
        getStateModel().setAgree(agree);
        sendStateModel();
    }

    private void performRegister() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair(UserRequest.PARAM_MOBILE,"mobile"));

        params.add(new BasicNameValuePair(UserRequest.PARAM_FIRSTNAME,getStateModel().getFirstname()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_LASTNAME,getStateModel().getLastname()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_EMAIL,getStateModel().getEmail()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_TELEPHONE,getStateModel().getTelephone()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_ADDRESS_1,getStateModel().getAddress_1()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_CITY,getStateModel().getCity()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_COUNTRY,getStateModel().getCountry_id().toString()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_ZONE,getStateModel().getZone().toString()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_PASSWORD,getStateModel().getPassword()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_CONFIRM,getStateModel().getConfirm()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_AGREE,getStateModel().getConfirm()));
        params.add(new BasicNameValuePair(UserRequest.PARAM_FAX,""));
        params.add(new BasicNameValuePair(UserRequest.PARAM_COMPANY,""));
        params.add(new BasicNameValuePair(UserRequest.PARAM_ADDRESS_2,""));
        params.add(new BasicNameValuePair(UserRequest.PARAM_POSTCODE,""));

        UserRequest request = new UserRequest(UserRequest.URL_REGISTER,params);

        PerformerRequest performer = new PerformerRequest(request,true){
            @Override
            public void onGettingToken(String[] token) {
                getStateModel().setShowProgress(false);
                getStateModel().setLoggedIn(token[0].equals("Регистрация прошла успешно"));
                getStateModel().setLostConnection(false);
                if(token.length>1)
                    getStateModel().message = token[1];
                sendStateModel();
            }
            @Override
            public void onLessConnection(String[] messages) {
                getStateModel().setShowProgress(false);
                getStateModel().setLoggedIn(null);
                getStateModel().setLostConnection(true);
                if(messages.length>1)
                    getStateModel().message = messages[1];

                sendStateModel();
            }

            @Override
            public void onCompleteRequest(JSONArray array) {

            }
        };
        performer.execute();
    }
}