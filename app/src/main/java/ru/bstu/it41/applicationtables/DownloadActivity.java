package ru.bstu.it41.applicationtables;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import ru.bstu.it41.applicationtables.DB.Database;
import ru.bstu.it41.applicationtables.main.R;

/**
 * Created by Герман on 29.10.2017.
 */


public class DownloadActivity extends Activity {
    //Переменные, которые можно в State перевести
    Database db = null;
    public void onDownloadDB(Boolean download){
        db.close();
        if(download){
            //Значит все было успешно загружено в базу данных
            //Открываем следующее активити
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        } else{
            //Иначе, скорее всего, были проблемы или с соединением или с кодом
            (findViewById(R.id.progressBar)).setVisibility(View.INVISIBLE);
            (findViewById(R.id.textLessConnection)).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        DownloadActivity its = this;
        db = new Database(this,its);

        db.open();
        db.upgradeDB();
        /*
        if(db.downloaded == null){
            //Тогда открываем следующее активити
            db.close();
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }*/
        //иначе тупо ждем, когда будет вызван onDownloadDB.
    }
}
