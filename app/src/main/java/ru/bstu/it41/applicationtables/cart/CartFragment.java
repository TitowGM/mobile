package ru.bstu.it41.applicationtables.cart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import etr.android.reamp.mvp.ReampFragment;
import etr.android.reamp.mvp.ReampPresenter;
import ru.bstu.it41.applicationtables.MainActivity;
import ru.bstu.it41.applicationtables.login.LoginFragmentPresenter;
import ru.bstu.it41.applicationtables.login.LoginFragmentState;
import ru.bstu.it41.applicationtables.main.R;

/**
 * Created by Герман on 16.11.2017.
 */

public class CartFragment extends ReampFragment<CartFragmentPresenter, CartFragmentState> {
    //Элементы управления

    //view-шки

    //

    @Override
    public void onStateChanged(CartFragmentState stateModel) {

    }
    @Override
    public ReampPresenter<CartFragmentState> onCreatePresenter() {
        return new CartFragmentPresenter();
    }

    @Override
    public CartFragmentState onCreateStateModel() {
        return new CartFragmentState();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.category_fragment_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}