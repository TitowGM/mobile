package ru.bstu.it41.applicationtables;

import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;

import etr.android.reamp.mvp.ReampPresenter;
import ru.bstu.it41.applicationtables.any.UserRequest;
import ru.bstu.it41.applicationtables.main.R;
import ru.bstu.it41.applicationtables.register.RegisterFragmentState;
import ru.istok.myapp.HTTPRequest.PerformerRequest;

/**
 * Created by Герман on 11.11.2017.
 */

public class MainActivityPresenter extends ReampPresenter<MainActivityState> {

    @Override
    public void onPresenterCreated() {
        super.onPresenterCreated();

        getStateModel().setAuthorized(false);
        sendStateModel();
    }

    public void onAuthorizedChanged(Boolean auth){
        getStateModel().setAuthorized(auth);
        sendStateModel();
    }

    public void exit(final TextView hello){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(UserRequest.PARAM_MOBILE,"mobile"));

        UserRequest request = new UserRequest(UserRequest.URL_LOGOUT,params);

        PerformerRequest performer = new PerformerRequest(request,true){
            @Override
            public void onGettingToken(String[] token) {
                hello.setText(token[0]);

                getStateModel().setAuthorized(false);
                sendStateModel();
            }
            @Override
            public void onLessConnection(String[] messages) {
                hello.setText(messages[1]);
            }

            @Override
            public void onCompleteRequest(JSONArray array) {
            }
        };
        performer.execute();
    }
}
