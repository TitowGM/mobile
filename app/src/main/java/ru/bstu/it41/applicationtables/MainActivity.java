package ru.bstu.it41.applicationtables;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.style.TypefaceSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.w3c.dom.Text;

import java.util.ArrayList;

import etr.android.reamp.mvp.ReampAppCompatActivity;
import etr.android.reamp.mvp.ReampPresenter;
import ru.bstu.it41.applicationtables.any.UserRequest;
import ru.bstu.it41.applicationtables.cart.CartFragment;
import ru.bstu.it41.applicationtables.empty.EmptyFragment;
import ru.bstu.it41.applicationtables.login.LoginFragment;
import ru.bstu.it41.applicationtables.main.R;
import ru.bstu.it41.applicationtables.register.RegisterFragment;
import ru.bstu.it41.applicationtables.register.RegisterFragmentPresenter;
import ru.bstu.it41.applicationtables.register.RegisterFragmentState;
import ru.istok.myapp.HTTPRequest.PerformerRequest;

public class MainActivity extends ReampAppCompatActivity<MainActivityPresenter, MainActivityState>
        implements NavigationView.OnNavigationItemSelectedListener {
    //Переменные, которые можно в State перевести
    ViewGroup container;
    MenuItem item_exit;
    MenuItem item_enter;
    MenuItem item_register;
    MenuItem item_cart;

    @Override
    public void onStateChanged(MainActivityState stateModel) {
        item_exit.setVisible(stateModel.getAuthorized());
        item_enter.setVisible(!stateModel.getAuthorized());
        item_register.setVisible(!stateModel.getAuthorized());
        item_cart.setVisible(stateModel.getAuthorized());
    }

    @Override
    public ReampPresenter<MainActivityState> onCreatePresenter() {
        return new MainActivityPresenter();
    }

    @Override
    public MainActivityState onCreateStateModel() {
        return new MainActivityState();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        View view = new View(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        TextView txt = (TextView) findViewById(R.id.hello);
        txt.setTypeface(Typeface.create("Papirus", Typeface.BOLD));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();

        //И инициализация этих переменных
        container = (ViewGroup) findViewById(R.id.container);

        item_exit = menu.findItem(R.id.nav_exit);

        //item_exit.setTitle(wrapInSpan("Выйти"));
        item_enter = menu.findItem(R.id.nav_enter);
        item_register = menu.findItem(R.id.nav_register);
        item_cart = menu.findItem(R.id.nav_cart);

        //Изменение шрифта у пунктов меню
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i); //for aapplying a font to subMenu ...
            applyFontToMenuItem(mi);
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            } //the method we have create in activity applyFontToMenuItem(mi); }
        }
    }

        @Override
        public void onBackPressed () {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @Override
        public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.main, menu);
            //item_exit= menu.add(0, 1, 0,"Logout");

            return true;
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }


        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected (MenuItem item){
            // Handle navigation view item clicks here.
            int id = item.getItemId();
            Fragment fragment = null;
            Class fragmentClass = null;

            switch (id) {
                case R.id.nav_catalog: {
                /*container = (LinearLayout) findViewById(R.id.container_category);
                fragmentClass = CategoryFragment.class;*/
               /* mytask=new UpdateTask(this);
                mytask.execute("http://"+ip+"/index.php?route=product/category&path=71&mobile=1");*/
                    break;
                }
                case R.id.nav_cart: {
                    container = (LinearLayout) findViewById(R.id.container_register);
                    fragmentClass = CartFragment.class;
                    break;
                }
                case R.id.nav_register: {
                    container = (LinearLayout) findViewById(R.id.container_register);
                    fragmentClass = RegisterFragment.class;
                    break;
                }
                case R.id.nav_enter: {
                    container = (LinearLayout) findViewById(R.id.container_login);
                    fragmentClass = LoginFragment.class;
                    break;
                }
                case R.id.nav_options: {

                    break;
                }
                case R.id.nav_view: {

                    break;
                }
                case R.id.nav_exit: {
                    getPresenter().exit((TextView) findViewById(R.id.hello));
                    container = (ConstraintLayout) findViewById(R.id.container_empty);
                    fragmentClass = EmptyFragment.class;
                    break;
                }
            }
            try {
                fragment = (Fragment) fragmentClass.newInstance();

                // Вставляем фрагмент, заменяя текущий фрагмент
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                // Выделяем выбранный пункт меню в шторке
                item.setChecked(true);
                // Выводим выбранный пункт в заголовке
                setTitle(item.getTitle());
            } catch (Exception e) {
                Log.d("act_main", "ошибка при выборе фрагмента:");
                e.printStackTrace();
            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.END);
            return true;
        }

        public static class SimpleTextWatcher implements TextWatcher {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }

    private CharSequence wrapInSpan(CharSequence value) {
        SpannableStringBuilder sb = new SpannableStringBuilder(value);
        sb.setSpan(Typeface.create("Times New Romans", Typeface.BOLD), 0, value.length(), 0);
        return sb;
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.create("Times New Romans", Typeface.BOLD);
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

}
