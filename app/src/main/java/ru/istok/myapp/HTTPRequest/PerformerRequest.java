package ru.istok.myapp.HTTPRequest;

import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Герман on 05.07.2017.
 */

public abstract class PerformerRequest extends AsyncTask<String, String, String> {
    private Request request = null;
    public boolean getToken = false;
    private String[] token;
    private JSONArray array;
    public Object buffer;

    abstract public void onCompleteRequest(JSONArray array);

    abstract public void onLessConnection(String[] messages);

    abstract public void onGettingToken(String[] token);

    public PerformerRequest(Request request)
    {
        this.request = request;
    }
    public PerformerRequest(Request request, boolean getToken)
    {
        this.request = request;
        this.getToken = getToken;
    }
    public PerformerRequest(Request request, boolean getToken,Object buffer)
    {
        this.request = request;
        this.getToken = getToken;
        this.buffer = buffer;
    }

    /*public PerformerRequest() throws Exception {
        throw new Exception("Необходимо вызвать конструктор с параметром");
    }*/

    protected void onPreExecute() {
        super.onPreExecute();
    }
    protected String doInBackground(String... args) {
        if(getToken)
            token = request.getToken();
        else
            array = request.commit();
        return null;
    }
    protected void onPostExecute(String file_url) {
        if(!getToken)
            try {
                if(array != null) {
                    if (array.getString(0).equals("Нет соединения")) {
                        onLessConnection(new String[]{array.getString(0)});
                    } else
                        onCompleteRequest(array);
                }
                else
                    onLessConnection(new String[]{"Данные не в формате json или их вовсе нет."});
            } catch (JSONException e) {
                e.printStackTrace();
            }
        else {
            try {
                    if(token[0].equals("Нет соединения"))
                        onLessConnection(token);
                    else
                        onGettingToken(token);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
