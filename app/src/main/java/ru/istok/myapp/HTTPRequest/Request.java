package ru.istok.myapp.HTTPRequest;

import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.json.JSONArray;

import java.util.ArrayList;

import ru.istok.myapp.HTTPRequest.parser.JSONParser;

/**
 * Created by Герман on 05.07.2017.
 */

public class Request {
    public String url;
    public String method;
    public ArrayList<NameValuePair> params;
    protected static CookieStore cookieStore = new BasicCookieStore();

    public CookieStore getCookieStore(){
        return cookieStore;
    }
    public Request(){

    }

    public Request(String url, String method,ArrayList<NameValuePair> params, CookieStore cookieStore)
    {
        this.url = url;
        this.params = params;
        this.method = method;
        this.cookieStore = cookieStore;
    }

    public Request(String url, String method,ArrayList<NameValuePair> params)
    {
        this.url = url;
        this.params = params;
        this.method = method;
    }

    public Request(String url)
    {
        this.url = url;
        this.params = new ArrayList<NameValuePair>();
        this.method = "POST";
    }

    //default method - POST
    public Request(String url,ArrayList<NameValuePair> params)
    {
        this.url = url;
        this.params = params;
        this.method = "POST";
    }

    public JSONArray commit(){
        try {
            JSONParser jParser = new JSONParser();
            return jParser.makeHttpRequest(url, method, params, cookieStore);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public String[] getToken()
    {
        JSONParser jParser = new JSONParser();
        return jParser.makeAuthorRequest(url, params,cookieStore);
    }
}
