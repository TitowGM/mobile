package ru.istok.myapp.HTTPRequest.parser;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JSONParser {

    static InputStream is = null;
    static JSONArray jObj = null;
    static String json = "";

    // constructor
    public JSONParser() {

    }

    // метод получение json объекта по url
    // используя HTTP запрос и методы POST или GET
    public JSONArray makeHttpRequest(String url, String method, List<NameValuePair> params,CookieStore cookieStore) throws JSONException {

        // Создаем HTTP запрос
        try {

            // проверяем метод HTTP запроса
            if(method == "POST"){
                HttpContext localContext = new BasicHttpContext();
                localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);


                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8")/*new UrlEncodedFormEntity(params)*/);

                HttpResponse httpResponse = httpClient.execute(httpPost,localContext);
                HttpEntity httpEntity = httpResponse.getEntity();

                is = httpEntity.getContent();

            }else if(method == "GET"){
                HttpContext localContext = new BasicHttpContext();
                localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString = URLEncodedUtils.format(params, "utf-8");
                //url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);

                //httpGet.addHeader("Content-Type","text/html; charset=UTF-8");

                HttpResponse httpResponse = httpClient.execute(httpGet,localContext);
                HttpEntity httpEntity = httpResponse.getEntity();
                //is = httpEntity.getContent();

                //json = EntityUtils.toString(httpEntity,"UTF-8");
                is = httpEntity.getContent();
            }

        }catch(Exception e) {
            e.printStackTrace();
            jObj.put(0,"Нет соединения");
            return jObj;
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // пытаемся распарсить строку в JSON объект
        try {
            //jObj = new JSONObject(json.substring(1,json.length()-2));
            jObj = new JSONArray(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // возвращаем JSON строку
        return jObj;
    }

    public String[] makeAuthorRequest(String url, List<NameValuePair> params, CookieStore cookieStore)
    {
        String[] array = new String[2];
        try {
            HttpContext localContext = new BasicHttpContext();
            localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));

            DefaultHttpClient httpClient = new DefaultHttpClient();

            /*httpClient.setRedirectHandler(new DefaultRedirectHandler() {
                @Override
                public boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
                    return false;
                }
            });*/

            HttpResponse httpResponse = httpClient.execute(httpPost,localContext);

            StatusLine status = httpResponse.getStatusLine();

            HttpEntity httpEntity = httpResponse.getEntity();

            String str = EntityUtils.toString(httpEntity, "UTF-8");

            array[0] = str;
            array[1] = str;

        }
        catch(Exception e) {
            e.printStackTrace();
            array[0] = "Нет соединения";
            array[1] = e.getMessage();
        }
        return array;
    }
}
