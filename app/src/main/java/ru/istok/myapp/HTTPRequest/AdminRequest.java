package ru.istok.myapp.HTTPRequest;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 * Created by Герман on 05.07.2017.
 */

public class AdminRequest extends Request {
    //private static String prefix = "http://192.168.2.107";
    //private static String prefix = "http://192.168.137.205/admin";
    //private static String prefix = "http://dementev2.ru/admin";
    private static String prefix = "http://istok31.ru/practic/dementev2.ru/admin";
    public static final String URL_AUTHORIZE = prefix+"/index.php?route=common/login";
    public static final String URL_ALL_CATEGORIES = prefix+"/index.php?route=catalog/category&mobile=1";
    public static final String URL_ALL_GOODS = prefix+"/index.php?route=catalog/product&mobile=1";
    public static final String URL_STORES = prefix +"/index.php?route=setting/store&mobile=1";
    public static final String URL_GRAB= prefix +"/index.php?route=catalog/grab/grab&mobile=1";
    public static final String URL_IMAGES= "http://istok31.ru/practic/dementev2.ru/image/";

    public AdminRequest(String url, ArrayList<NameValuePair> params, String token){
        super(url+"&token="+token,params);
    }
    public AdminRequest(String url, ArrayList<NameValuePair> params){
        super(url,params);
    }
    public AdminRequest(String url,String token){
        super(url+"&token="+token);
    }
    public AdminRequest(String url,String token,String user_id){
        super(url+"&token="+token+"&user_id="+user_id);
    }
    public AdminRequest(String url){
        super(url);
    }
}
