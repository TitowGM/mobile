package ru.istok.myapp;


/**
 * Created by Герман on 05.07.2017.
 */

abstract public class AdminRequestActivity {
    public static final byte LOAD_ALL_CATEGORIES = 1;
    public static final byte LOAD_GOOD_FROM_CATEGORY = 2;
    public static final byte LOAD_ALL_GOODS = 3;
    public static final byte LOAD_STORES = 4;
    public static final byte LOAD_GRABS = 5;

    protected static final String TAG_SUCCESS = "success";
    protected static final String TAG_PRODUCTS = "categories";
    protected static final String TAG_PRODUCT_ID = "product_id";
    protected static final String TAG_CATEGORY_ID = "category_id";
    protected static final String TAG_ParentID = "parent_id";
    protected static final String TAG_STORE_ID = "store_id";
    protected static final String TAG_IMAGE = "image";

    protected static final String TAG_NAME = "name";
    protected static final String TAG_Date_Added = "date_added";
    protected static final String TAG_Date_Changed = "date_modified";
    protected static final String TAG_Date = "date";
    protected static final String TAG_Mail = "mail";
    protected static final String TAG_SortOrder= "sort_order";
    protected static final String TAG_URL = "url";

    protected static final String TAG_PRODUCTID = "product_id";
    protected static final String TAG_DESCRIPTION = "description";
    protected static final String TAG_PRICE = "price";
    protected static final String TAG_COUNT = "quantity";
}

