package com.nord51.mag03;

//import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener  {

    UpdateTask mytask;
    int wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
    LinearLayout container;
    String url;
    Map<Integer, String> map=new Hashtable<Integer, String>();
    Context context;
    String ip="192.168.1.4";
    String id="";

    public static void onCategoryClick()
    {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context=this;


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;
        switch(id)
        {
            case R.id.nav_catalog:
            {
                container = (LinearLayout) findViewById(R.id.container_category);
                fragmentClass = CategoryFragment.class;
               /* mytask=new UpdateTask(this);
                mytask.execute("http://"+ip+"/index.php?route=product/category&path=71&mobile=1");*/
                break;
            }
            case R.id.nav_cart:
            {

                break;
            }
            case R.id.nav_special:
            {

                break;
            }
            case R.id.nav_register:
            {

                break;
            }
            case R.id.nav_enter:
            {

                break;
            }
            case R.id.nav_options:
            {

                break;
            }
            case R.id.nav_adresses:
            {
                break;
            }
            case R.id.nav_view:
            {

                break;
            }
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Вставляем фрагмент, заменяя текущий фрагмент
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        // Выделяем выбранный пункт меню в шторке
        //item.setChecked(true);
        // Выводим выбранный пункт в заголовке
        setTitle(item.getTitle());
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class UpdateTask extends AsyncTask<String, Void, JSONArray> {

        Context context;

        public UpdateTask(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... urls) {
            return loadJSON(urls[0]);
        }

        public JSONArray loadJSON(String url) {

            JSONParser jParser = new JSONParser();
            // здесь параметры необходимые в запрос добавляем
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("", ""));
            // посылаем запрос методом GET
            JSONArray jsonarray = jParser.makeHttpRequest(url, "GET", params);
            return jsonarray;
        }

        @Override
        protected void onPostExecute(JSONArray jsonData) {

            if (jsonData != null) {
                super.onPostExecute(jsonData);
                drawMain(jsonData);
            } else {
                Toast.makeText(this.context,"error",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void drawMain(JSONArray jsonarray)
    {
        for(int i=0; i < jsonarray.length(); i++)
        {
            try {

                JSONObject jObj = jsonarray.getJSONObject(i);
                LinearLayout layout = (LinearLayout) findViewById(R.id.container_category);
                LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                        wrapContent, wrapContent);
                lParams.gravity = Gravity.LEFT;
                TextView btnNew = new TextView(this);
                //  btnNew.setBackgroundColor(Color.LTGRAY);
                btnNew.setTextSize(24);
                btnNew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        url="http://"+ip+"/index.php?route=product/category&path="+v.getId()+"&mobile=2";
                       UpdateTask1 mytask1;
                        mytask1=new UpdateTask1(context);
                        mytask1.execute(url);
                        Fragment fragment = null;
                        Class fragmentClass = null;
                        fragmentClass = CategoryFragment.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                        //  setTitle(item.getTitle());
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
                btnNew.setClickable(true);
                btnNew.setText(jObj.getString("name"));
                btnNew.setId(Integer.parseInt(jObj.getString("category_id")));
                layout.addView(btnNew,lParams);

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

        }
    }


    public class UpdateTask1 extends AsyncTask<String, Void, JSONArray> {
        Context context;

        public UpdateTask1(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... urls) {
            return loadJSON(urls[0]);
        }

        public JSONArray loadJSON(String url) {

            JSONParser jParser = new JSONParser();
            // здесь параметры необходимые в запрос добавляем
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("", ""));
            // посылаем запрос методом GET
            JSONArray jsonarray = jParser.makeHttpRequest(url, "GET", params);
            return jsonarray;
        }

        @Override
        protected void onPostExecute(JSONArray jsonData) {
            if (jsonData != null) {
                super.onPostExecute(jsonData);
                drawTwo(jsonData);
            } else {
                Toast.makeText(null, "error", Toast.LENGTH_SHORT).show();

            }
        }

    }

    public void drawTwo(JSONArray jsonarray)
    {

        for(int i=0; i < jsonarray.length(); i++)
        {
            try {
                JSONObject jObj = jsonarray.getJSONObject(i);
                LinearLayout layout = (LinearLayout) findViewById(R.id.container_category);
                LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                        wrapContent, wrapContent);
                lParams.gravity = Gravity.LEFT;
                TextView btnNew = new TextView(this);

                btnNew.setTextSize(24);

                btnNew.setClickable(true);
                btnNew.setText(jObj.getString("name"));
                String sid=jObj.getString("href");
                int j=sid.length();
                String buf="";
                while(sid.charAt(j-1)!='_')
                {
                    buf+=sid.charAt(j-1);
                    j--;
                }
                buf= new StringBuffer(buf).reverse().toString();
                int iid=Integer.parseInt((buf));
                map.put(iid,sid);
                btnNew.setId(iid);
                btnNew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String href=map.get(v.getId());
                        int i=0;
                        while (href.charAt(i)!='_')
                        {
                            i++;
                        }
                        String buf="";
                        buf+=href.charAt(i-2);
                        buf+=href.charAt(i-1);
                        url="http://"+ip+"//index.php?route=product/category&path="+buf+"_"+v.getId()+"&mobile=3";
                        UpdateTask3 mytask3;
                        mytask3=new UpdateTask3(context);
                        mytask3.execute(url);
                        Fragment fragment = null;
                        Class fragmentClass = null;
                        fragmentClass = GoodsFragment.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                        //  setTitle(item.getTitle());
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
                layout.addView(btnNew,lParams);

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

        }
    }

    public class UpdateTask3 extends AsyncTask<String, Void, JSONArray> {
        Context context;

        public UpdateTask3(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... urls) {
            return loadJSON(urls[0]);
        }

        public JSONArray loadJSON(String url) {

            JSONParser jParser = new JSONParser();
            // здесь параметры необходимые в запрос добавляем
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("", ""));
            // посылаем запрос методом GET
            JSONArray jsonarray = jParser.makeHttpRequest(url, "GET", params);
            return jsonarray;
        }

        @Override
        protected void onPostExecute(JSONArray jsonData) {

            if (jsonData != null) {
                super.onPostExecute(jsonData);
                String res = "";
                drawGoods(jsonData);

            } else {
                Toast.makeText(null, "error", Toast.LENGTH_SHORT).show();

            }
        }

    }

    public void drawGoods(JSONArray jsonarray)
    {

        for(int i=0; i < jsonarray.length(); i++)
        {
            try {
                JSONObject jObj = jsonarray.getJSONObject(i);
                LinearLayout layout = (LinearLayout) findViewById(R.id.good_container);
                LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                        wrapContent, wrapContent);
                lParams.gravity = Gravity.CENTER;

                ImageView img= new ImageView(this);
                img.setImageResource(R.drawable.ic_menu_slideshow);


                String url_pic=jObj.getString("thumb");
                url_pic=url_pic.replace("dementev2.ru",ip);

                Picasso.with(context)
                        .load(url_pic)
                        .placeholder(R.drawable.ic_menu_slideshow)
                        .error(R.drawable.ic_menu_gallery)
                        .into(img);
                layout.addView(img,lParams);
                lParams.gravity = Gravity.LEFT;

                LinearLayout llay=new LinearLayout(this);
                llay.setOrientation(LinearLayout.HORIZONTAL);

                TextView btnNew = new TextView(this);
                btnNew.setTextSize(20);
                btnNew.setText("Название: ");
                llay.addView(btnNew,lParams);

                btnNew = new TextView(this);
                btnNew.setTextSize(20);
                btnNew.setText(jObj.getString("name"));
                btnNew.setId(jObj.getInt("product_id"));
                llay.addView(btnNew,lParams);
                layout.addView(llay);

                btnNew = new TextView(this);
                btnNew.setTextSize(20);
                String description = jObj.getString("description");
                String str=description.replaceAll("\\t|\\r|\\n","");
                str=str.replaceFirst("\\s","");
                btnNew.setText(str);
               // btnNew.setId(jObj.getInt("product_id"));
                layout.addView(btnNew,lParams);

                llay=new LinearLayout(this);
                llay.setOrientation(LinearLayout.HORIZONTAL);
                btnNew = new TextView(this);
                btnNew.setTextSize(20);
                btnNew.setText("Цена:");
                llay.addView(btnNew,lParams);

                btnNew = new TextView(this);
                btnNew.setTextSize(20);
                btnNew.setText(jObj.getString("price"));
                //btnNew.setId(jObj.getInt("product_id"));
                llay.addView(btnNew,lParams);
                layout.addView(llay);

                llay=new LinearLayout(this);
                llay.setOrientation(LinearLayout.HORIZONTAL);
                Button but = new Button(this);
                but.setText("Купить");
                but.setTextSize(14);
                llay.addView(but);
                but = new Button(this);
                but.setText("Лайк");
                but.setTextSize(14);
                llay.addView(but);
                but = new Button(this);
                but.setText("Сравнение");
                but.setTextSize(14);
                llay.addView(but);
                //layout.setId(Integer.parseInt(jObj.getString("product_id")));
                layout.addView(llay,lParams);
                id=jObj.getString("product_id");
                layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        url="http://"+ip+"/index.php?route=product/product&path=71_73&product_id="+id+"&mobile=4";
                        UpdateTask4 mytask4;
                        mytask4=new UpdateTask4(context);
                        mytask4.execute(url);
                        Fragment fragment = null;
                        Class fragmentClass = null;
                        fragmentClass = GoodFragment.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                        //  setTitle(item.getTitle());
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

        }
    }

    public class UpdateTask4 extends AsyncTask<String, Void, JSONArray> {
        Context context;

        public UpdateTask4(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... urls) {
            return loadJSON(urls[0]);
        }

        public JSONArray loadJSON(String url) {

            JSONParser jParser = new JSONParser();
            // здесь параметры необходимые в запрос добавляем
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("", ""));
            // посылаем запрос методом GET

            JSONArray jsonarray =  jParser.makeHttpRequest(url, "GET", params);
            return jsonarray;
        }

        @Override
        protected void onPostExecute(JSONArray jsonData) {

            if (jsonData != null) {
                super.onPostExecute(jsonData);
                drawGood(jsonData);

            } else {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();

            }
        }

    }
    public void drawGood(JSONArray jsonarray)
    {

            try {


                JSONObject jObj = jsonarray.getJSONObject(0);


                ImageView img= new ImageView(this);
                img.setImageResource(R.drawable.ic_menu_slideshow);


                String url_pic="http://"+ip+"/image/"+jObj.getString("image");
                // url_pic=url_pic.replace("dementev2.ru",ip);

                Picasso.with(context)
                        .load(url_pic)

                        .placeholder(R.drawable.ic_menu_slideshow)
                        .error(R.drawable.ic_menu_gallery)
                        .into(img);
                LinearLayout layout1 = (LinearLayout) findViewById(R.id.good_container1);
                LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                        wrapContent, wrapContent);
               // lParams.gravity = Gravity.CENTER;



                layout1.addView(img);
                lParams.gravity = Gravity.LEFT;

                LinearLayout llay=new LinearLayout(this);
                llay.setOrientation(LinearLayout.HORIZONTAL);

                TextView btnNew = new TextView(this);
                btnNew.setTextSize(20);
                btnNew.setText("Название: ");
                llay.addView(btnNew,lParams);

                btnNew = new TextView(this);
                btnNew.setTextSize(20);
                btnNew.setText(jObj.getString("name"));
                btnNew.setId(jObj.getInt("product_id"));
                llay.addView(btnNew,lParams);
                layout1.addView(llay);

                btnNew = new TextView(this);
                btnNew.setTextSize(20);
                String description = jObj.getString("description");
                String str=description.replaceAll("\\t|\\r|\\n","");
                str=str.replaceFirst("\\s","");
                btnNew.setText(str);
                // btnNew.setId(jObj.getInt("product_id"));
                layout1.addView(btnNew,lParams);

                llay=new LinearLayout(this);
                llay.setOrientation(LinearLayout.HORIZONTAL);
                btnNew = new TextView(this);
                btnNew.setTextSize(20);
                btnNew.setText("Цена:");
                llay.addView(btnNew,lParams);

                btnNew = new TextView(this);
                btnNew.setTextSize(20);
                btnNew.setText(jObj.getString("price"));
                //btnNew.setId(jObj.getInt("product_id"));
                llay.addView(btnNew,lParams);
                layout1.addView(llay);

                llay=new LinearLayout(this);
                llay.setOrientation(LinearLayout.HORIZONTAL);
                Button but = new Button(this);
                but.setText("Купить");
                but.setTextSize(14);
                llay.addView(but);
                but = new Button(this);
                but.setText("Лайк");
                but.setTextSize(14);
                llay.addView(but);
                but = new Button(this);
                but.setText("Сравнение");
                but.setTextSize(14);
                llay.addView(but);

                layout1.addView(llay,lParams);

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }


    }

}
