package com.nord51.mag03;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NorD on 03.07.2017.
 */

public class CategoryFragment extends android.support.v4.app.Fragment {

    UpdateTask mytask;
    int wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
    LinearLayout llMain;
    Context context;
    String ip="192.168.1.4";
    String url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

      context=getActivity().getApplicationContext();

                mytask=new UpdateTask(getContext());
                mytask.execute("http://"+ip+"/index.php?route=product/category&path=71&mobile=1");


        return inflater.inflate(R.layout.category_fragment, container, false);
    }

    public class UpdateTask extends AsyncTask<String, Void, JSONArray> {

        Context context;

        public UpdateTask(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... urls) {
            return loadJSON(urls[0]);
        }

        public JSONArray loadJSON(String url) {

            JSONParser jParser = new JSONParser();
            // здесь параметры необходимые в запрос добавляем
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("", ""));
            // посылаем запрос методом GET
            JSONArray jsonarray = jParser.makeHttpRequest(url, "GET", params);
            return jsonarray;
        }

        @Override
        protected void onPostExecute(JSONArray jsonData) {

            if (jsonData != null) {
                super.onPostExecute(jsonData);
                drawMain(jsonData);
            } else {
                Toast.makeText(this.context,"error",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void drawMain(JSONArray jsonarray)
    {
        for(int i=0; i < jsonarray.length(); i++)
        {
            try {

                JSONObject jObj = jsonarray.getJSONObject(i);
                LinearLayout layout = (LinearLayout) getView().findViewById(R.id.container_category);
                LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                        wrapContent, wrapContent);
                lParams.gravity = Gravity.LEFT;
                TextView btnNew = new TextView(context);
                //  btnNew.setBackgroundColor(Color.LTGRAY);
                btnNew.setTextSize(24);
                btnNew.setClickable(true);
                btnNew.setText(jObj.getString("name"));
                btnNew.setId(Integer.parseInt(jObj.getString("category_id")));

                btnNew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.onCategoryClick();
                        url="http://"+ip+"/index.php?route=product/category&path="+v.getId()+"&mobile=2";

                        android.support.v4.app.Fragment fragment = null;
                        Class fragmentClass = null;
                        fragmentClass = SubCategoryFragment.class;
                        try {
                            fragment = (android.support.v4.app.Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().add(R.id.container, fragment).commit();
                        //  setTitle(item.getTitle());
                        DrawerLayout drawer = (DrawerLayout) getView().findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
                layout.addView(btnNew,lParams);

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

        }
    }


}
